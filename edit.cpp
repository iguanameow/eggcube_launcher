#include "edit.h"
#include "ui_edit.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edit)
{
    ui->setupUi(this);
}

void Edit::setInputStrings(QStringList &game,QStringList &stringPath,QStringList &stingCommand,int selection)   // Recieve data from parent
{
    ui->lineEdit_5->setText(game[selection]);
    ui->lineEdit_4->setText(stringPath[selection]);
    ui->lineEdit_3->setText(stingCommand[selection]);

    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }

    passedSelection=selection;
    passedNameOfProgram=&game;
    passedPath=&stringPath;
    passedCommands=&stingCommand;
}

Edit::~Edit()
{
    delete ui;
}

void Edit::on_pushButton_2_clicked()    // Save Button
{
    qDebug("Saving to Config File");
    QStringList outputGame;
    QStringList outputPath;
    QStringList outputCommands;

    for(int i=0; i<(*passedNameOfProgram).size(); i++)  // Refill all data
    {
        if(i!=passedSelection)  // Copy non-related data
        {
            outputGame.append((*passedNameOfProgram)[i]);
            outputPath.append((*passedPath)[i]);
            outputCommands.append((*passedCommands)[i]);
        }else{  // Get editted data
            outputGame.append(ui->lineEdit_5->text());
            outputPath.append(ui->lineEdit_4->text());
            outputCommands.append(ui->lineEdit_3->text());
        }
    }
    (*passedNameOfProgram).clear();
    (*passedPath).clear();
    (*passedCommands).clear();
    (*passedNameOfProgram)=outputGame;
    (*passedPath)=outputPath;
    (*passedCommands)=outputCommands;
    writeConfigFile();
    close();
}

void Edit::writeConfigFile()    // Write back to config file
{
    #ifdef Q_OS_MACOS
        QFile file("../../../ConfigFile.txt");
    #else
        QFile file("ConfigFile.txt");
    #endif
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    out << "$,numCases,"+QString::number((*passedNameOfProgram).size()) << endl; // Writing header
    for(int i=0;i<(*passedNameOfProgram).size();i++)    // Rewrite all data
    {
        out << QString::number(i)+",name,"+(*passedNameOfProgram)[i] << endl;
        out << QString::number(i)+",path,"+(*passedPath)[i] << endl;
        out << QString::number(i)+",command,"+(*passedCommands)[i] << endl;
    }
    file.close();
}

void Edit::on_lineEdit_5_textChanged()
{
    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }
}

void Edit::on_lineEdit_4_textChanged()
{
    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }
}


void Edit::on_pushButton_6_clicked()    // Edit Path Button
{
qDebug("Opening Path");
#ifdef Q_OS_MACOS
    QString filename = QFileDialog::getOpenFileName(this,tr("Open Game Path"), "/Applications", "app files (*.app);; All files (*)");
    if(filename.isNull())
    {
          filename="";
    }else{
        filename=filename+"/Contents/MacOS/launcher";
    }
    ui->lineEdit_4->setText(filename);
#elif _WIN32
    QString filename = QFileDialog::getOpenFileName(this,tr("Open Game Path"), "C://", "bat files (*.bat);; exe files (*.exe);; All files (*)");
    if(filename.isNull())
        {
          filename="";
        }
    ui->lineEdit_4->setText(filename);
#else
    QString filename = QFileDialog::getOpenFileName(this,tr("Open Game Path"), "/home", "shell files (*.sh);; All files (*)");
    if(filename.isNull())
        {
          filename="";
        }
    ui->lineEdit_4->setText(filename);
#endif
}
