apt update -y
apt install -y		\
  build-essential	\
  qt5-default		\
  qtcreator			\
  clang				\
  llvm				\
  make				\
  curl				\
  git				\
  g++
