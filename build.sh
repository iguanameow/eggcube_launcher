ConcurProc=$(CONCURRENCY_LEVEL=$(getconf _NPROCESSORS_ONLN))
NumProc=$(($(getconf _NPROCESSORS_ONLN)+1))
./build-clean.sh
qmake EggCube_Launcher.pro $ConcurProc
time $ConcurProc make -j $NumProc
chmod +x EggCube_Launcher
echo ""
./EggCube_Launcher
