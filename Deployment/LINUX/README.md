Add the Qt directory, binary, and library path as so:
```
qtchooser -install qt5.8 /home/$USER/Qt5.8.0/5.8/gcc_64/bin/qmake
export QT_SELECT=qt5.8
QTDIR=/home/$USER/Qt5.8.0/5.8/gcc_64/
PATH=$PATH:$QTDIR/bin
LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH
QMAKESPEC=$QTDIR/mkspecs/linux-g++
QT_PLUGIN_PATH=$QTDIR/plugins
export QTDIR PATH LD_LIBRARY_PATH QMAKESPEC QT_PLUGIN_PATH QT_SELECT=qt5.8
```

Deploy to Linux by downloading and running the [linuxdeployqt toolkit](https://github.com/probonopd/linuxdeployqt).

Video on how to do it here: https://www.youtube.com/watch?v=PDzlT_ODpM8
```
chmod a+x linuxdeployqt-continuous-x86_64.AppImage
./linuxdeployqt-continuous-x86_64.AppImage /path/to/build folder/AssaultCube_Launcher
```

For 32bit linux compile the linuxdeployqt toolkit and use one of the 32bit linux online installers and follow the instructions above.
