For Windows if you use Qt5 you need to run the windeploymentqt tool on the binary from the command prompt.
```
cd C:\
Qt\Qt5.8.0\5.8\mingw53_32\bin\windeployqt.exe C:\path\to\binary.exe
```

Then copy the MinGW libraries.
```
libgcc_s_dw2-1.dll
libstdc++-6.dll
libwinpthread-1.dll
```

If using Qt4.8.6 or Qt4.8.7 you won't need to use the windeployqt tool, just copy the missing Qt and MinGW libraries.
```
libgcc_s_dw2-1.dll
libstdc++-6.dll
libwinpthread-1.dll
QtCore4.dll
QtGui4.dll
```
