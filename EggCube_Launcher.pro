#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T18:37:43
#
#-------------------------------------------------

ConcurProc=$(CONCURRENCY_LEVEL=$(getconf _NPROCESSORS_ONLN))
NumProc=$(($(getconf _NPROCESSORS_ONLN)+1))

QT       += core xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EggCube_Launcher
TEMPLATE = app


SOURCES += main.cpp\
	launcher.cpp \
    add.cpp \
    edit.cpp \
    servers.cpp

TRANSLATIONS += Translations/ar.ts \
	Translations/bg.ts \
	Translations/cn.ts \
	Translations/de.ts \
	Translations/es.ts \
	Translations/fa.ts \
	Translations/fr.ts \
	Translations/hr.ts \
	Translations/il.ts \
	Translations/in.ts \
	Translations/it.ts \
	Translations/jp.ts \
	Translations/kr.ts \
	Translations/my.ts \
	Translations/nl.ts \
	Translations/pt.ts \
	Translations/ru.ts \
	Translations/se.ts \
	Translations/th.ts \
    Translations/tr.ts \
    Translations/za.ts

HEADERS  += launcher.h \
    add.h \
    edit.h \
    servers.h

FORMS    += launcher.ui \
    add.ui \
    edit.ui \
    servers.ui

QMAKE_LFLAGS += -Wl,-rpath,"'\$$ORIGIN'"
QMAKE_CXXFLAGS += -Os -std=c++11	#linux use -pthread
									#windows use -mthreads

RESOURCES += \
    resources.qrc

DISTFILES +=

target.path += build/
target.files += $$TARGET \
	./Deployment/PACKAGING/README.html \
	./Deployment/PACKAGING/icon.png

linux {
        target.files += ./Deployment/PACKAGING/AC_Luncher.desktop
}

INSTALLS += target
