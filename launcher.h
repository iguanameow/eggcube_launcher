#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>
#include <QNetworkReply>

namespace Ui {
class launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = 0);
    ~Launcher();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_actionAssaultCube_triggered();

    void on_actionGema_Forums_triggered();

    void on_actionIguana_Site_triggered();

    void on_actionDocs_triggered();

    void on_actionWiki_triggered();

    void on_actionAkimbo_triggered();

    void on_actionQuadropolis_triggered();

    void on_actionAkimbo_US_triggered();

    void on_actionIguana_RU_triggered();

    void on_actiontm_FR_triggered();

    void on_actionAssaultCube_2_triggered();

    void on_actionLauncher_triggered();

    void on_actionDownload_1_triggered();

    void on_actionDownload_2_triggered();

    void on_actionQt_triggered();

    void refreshCombobox();

    void readConfigFile();

    void writeConfigFile();

    void on_comboBox_2_currentIndexChanged();

    void doDownload(const QUrl &url);

    void downloadFinished(QNetworkReply *reply);

    void sslErrors(const QList<QSslError> &sslErrors);

    void updateCheck();

    void connectionTimeOut();

    void on_pushButton_6_clicked();

private:
    Ui::launcher *ui;
    QTimer *timer;
};

#endif // LAUNCHER_H
