#include "servers.h"
#include "ui_servers.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QTextCodec>
#include <QString>
#include <QTableWidget>
#include <QUdpSocket>
#include <QTimer>
QStringList masterServerList;
QUdpSocket *udpSocket;

bool isInFirstResponse=true;
int currentByteArrayIndex=0;

int getIntFromDatagram(QStringList s);
QString getStringFromDatagram(QStringList s);

struct ServersInfo{
    int portocol;// should be 1201
    int mode;
    int numPlayers;
    int numMinRemaining;
    QString mapName;
    QString serverDescription;
    int maxPlayers;
    int masterMode;
    bool hasPassword;
    QString infoLang;
    int type;//should be 2
    QStringList players;

    bool isInfoSet=false;
    bool isSet=false;
};
QVector<ServersInfo> allServersInfo;

QString mode_names[] = { "DEMO", "TDM", "coop", "DM", "SURV", "TSURV", "CTF", "PF", "BTDM", "BDM", "LSS",
    "OSOK", "TOSOK", "BOSOK", "HTF", "TKTF", "KTF", "TPF", "TLSS", "BPF", "BLSS", "BTSURV", "BTOSOK"};

int currentRow=0;

int currentTotallRefreshTime=80;
int currentNumberUnsetServers;
int totalTimeOutNumberRefreshes=1200;
int currentRefreshNumber=0;
int currentRefreshNumberOneServer=0;

bool hasChangeGained=false;//If connection be too slow in which no server could be retrieved in timeout period, increase timeout "currentTotallRefreshTime"

bool isBusy=false;


QString buttomPlainTextViewString="";

Servers::Servers(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::servers)
{
    ui->setupUi(this);
    udpSocket = new QUdpSocket(this);
    connect( udpSocket, SIGNAL(readyRead()),this, SLOT(readUDPData()));

    ui->tableWidget->horizontalHeader()->setStyleSheet("color: grey");
    ui->tableWidget->verticalHeader()->setStyleSheet("color: grey");
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->plainTextEdit->setReadOnly(true);

    // Resizing table columns
    ui->tableWidget->setColumnWidth(0, 55);
    ui->tableWidget->setColumnWidth(1, 55);
    ui->tableWidget->setColumnWidth(2, 160);
    ui->tableWidget->setColumnWidth(3, 230);
    ui->tableWidget->setColumnWidth(4, 110);
    ui->tableWidget->setColumnWidth(5, 55);

    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(downloadFinished(QNetworkReply*)));
    abortTimer = new QTimer(this);
    secondTimer = new QTimer(this);
    refreshTimer=new QTimer(this);
    connect(abortTimer, SIGNAL(timeout()), this, SLOT(abortUDPConnection()));
    connect(secondTimer, SIGNAL(timeout()), this, SLOT(doubleConnection()));
    connect(refreshTimer, SIGNAL(timeout()), this, SLOT(checkConnectionStatus()));
    QString rawURL="http://ms.cubers.net/retrieve.do?action=list&name=iguana_launcher&version=1202";

    QUrl url = QUrl::fromEncoded(rawURL.toLocal8Bit());

    QNetworkRequest request(url);
    QNetworkReply *reply = manager->get(request);

    #ifndef QT_NO_SSL
        connect(reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)));
    #endif

    currentDownloads.append(reply);
}

void Servers::on_tableWidget_cellPressed(int row,int column)
{
    abortUDPConnection();

    bool isClicked=!refreshTimer->isActive();
    if(isBusy==false)
    {
        isBusy=true;
        if(isClicked==true)
        {
            currentRow=row;

            udpSocket = new QUdpSocket(this);

            connect( udpSocket, SIGNAL(readyRead()),this, SLOT(readUDPData()));
            QString server=masterServerList.at(currentRow);//ui->tableWidget->selectionModel()->selectedRows().at(0).row());
            QStringList info=server.split(":");
            QString address=info.at(0);
            QString port=info.at(1);

            bool success=udpSocket->bind(QHostAddress::LocalHost, port.toUInt()+1);//port.toUInt());
//            qDebug()<<success;

            QByteArray datagramFirst;//("\x01\x02\x65\x6E");//temporary, this is main code
            datagramFirst.resize(4);
            datagramFirst[0]=0x01;
            datagramFirst[1]=0x02;
            datagramFirst[2]=0x65;
            datagramFirst[3]=0x6E;

            udpSocket->writeDatagram(datagramFirst, QHostAddress(address), port.toUInt()+1);

            udpSocket = new QUdpSocket(this);

            connect( udpSocket, SIGNAL(readyRead()),this, SLOT(readUDPData()));
            server=masterServerList.at(currentRow);//ui->tableWidget->selectionModel()->selectedRows().at(0).row());
            info=server.split(":");
            address=info.at(0);
            port=info.at(1);

            udpSocket->bind(QHostAddress::LocalHost, port.toUInt()+1);//port.toUInt());

            QByteArray datagramDouble;//("\x01\x02\x65\x6E");//temporary, this is main code
            datagramDouble.resize(4);
            datagramDouble[0]=0x01;
            datagramDouble[1]=0x02;
            datagramDouble[2]=0x65;
            datagramDouble[3]=0x6E;

            udpSocket->writeDatagram(datagramDouble, QHostAddress(address), port.toUInt()+1);

            isInFirstResponse=true;

            abortTimer->start(2000);
        }else{
            if(allServersInfo[row].isSet==false)
            {
                udpSocket = new QUdpSocket(this);

                connect( udpSocket, SIGNAL(readyRead()),this, SLOT(readUDPData()));

                QString server=masterServerList.at(currentRow);//ui->tableWidget->selectionModel()->selectedRows().at(0).row());
                QStringList info=server.split(":");
                QString address=info.at(0);
                QString port=info.at(1);

                bool success=udpSocket->bind(QHostAddress::LocalHost, port.toUInt()+1);//port.toUInt());
//                qDebug()<<success;

                QByteArray datagramFirst;//("\x01\x02\x65\x6E");//temporary, this is main code
                datagramFirst.resize(4);
                datagramFirst[0]=0x01;
                datagramFirst[1]=0x02;
                datagramFirst[2]=0x65;
                datagramFirst[3]=0x6E;

                udpSocket->writeDatagram(datagramFirst, QHostAddress(address), port.toUInt()+1);

                udpSocket = new QUdpSocket(this);

                connect( udpSocket, SIGNAL(readyRead()),this, SLOT(readUDPData()));

                currentRow=row;
                server=masterServerList.at(currentRow);//ui->tableWidget->selectionModel()->selectedRows().at(0).row());
                info=server.split(":");
                address=info.at(0);
                port=info.at(1);

                udpSocket->bind(QHostAddress::LocalHost, port.toUInt()+1);//port.toUInt());

                QByteArray datagramDouble;//("\x01\x02\x65\x6E");//temporary, this is main code
                datagramDouble.resize(4);
                datagramDouble[0]=0x01;
                datagramDouble[1]=0x02;
                datagramDouble[2]=0x65;
                datagramDouble[3]=0x6E;

                udpSocket->writeDatagram(datagramDouble, QHostAddress(address), port.toUInt()+1);

                isInFirstResponse=true;

                float eachServerTime=(float)currentTotallRefreshTime/(float)currentNumberUnsetServers;
                if(eachServerTime>2)
                {
                    eachServerTime=2;
                }
                abortTimer->start(eachServerTime*1000);
            }else{
                abortUDPConnection();
            }
        }
    }
    isBusy=false;
}

void Servers::readUDPData()
{
    isBusy=true;
    currentByteArrayIndex=0;
    QByteArray datagram;
    while (udpSocket->hasPendingDatagrams()) {
            datagram.resize(udpSocket->pendingDatagramSize());
//            qDebug()<<udpSocket->pendingDatagramSize();
            QHostAddress sender;
            quint16 senderPort;
            udpSocket->readDatagram(datagram.data(), datagram.size(),
                                    &sender, &senderPort);

            QStringList s;

            for(int i = 0; i < datagram.size(); i++)
            {
                s.append("0x"+QString::number(static_cast<unsigned char>(datagram[i]),16).toUpper().rightJustified(2,'0')+ " ");
            }
//            qDebug()<<s;
            if(isInFirstResponse==true)
            {
                getServerInfo(s);
            }else{
                getPlayerInfo(s);
            }
        }
        if(datagram.size()>0)
        {
//            qDebug() << "Num players";
//            qDebug() << allServersInfo[currentRow].players.size();
//            qDebug() << allServersInfo[currentRow].numPlayers;
//            qDebug() << "Num players";
            if(allServersInfo[currentRow].players.size()==allServersInfo[currentRow].numPlayers)
            {
                isBusy=false;
                hasChangeGained=true;
                allServersInfo[currentRow].isSet=true;
                if(allServersInfo[currentRow].players.size()>0)
                {
                    for(int i=0;i<allServersInfo.size();i++)
                    {


                        if(allServersInfo[i].players.size()==0)
                        {
                            if(i<currentRow)
                            {
                                ServersInfo temp;
                                temp=allServersInfo[i];
                                allServersInfo[i]=allServersInfo[currentRow];
                                allServersInfo[currentRow]=temp;
                                QStringList currentRowStringList;
                                QStringList emptyRowStringList;
                                for(int j=0;j<ui->tableWidget->columnCount();j++)
                                {
                                    currentRowStringList.append(ui->tableWidget->item(currentRow, j)->text());
                                    emptyRowStringList.append(ui->tableWidget->item(i, j)->text());
                                    ui->tableWidget->item(i, j)->setText(currentRowStringList.at(j));
                                    ui->tableWidget->item(currentRow, j)->setText(emptyRowStringList.at(j));
                                }
                                QString currentRowMasterServer=masterServerList[currentRow];
                                QString emptyRowMasterServer=masterServerList[i];
                                masterServerList[i]=currentRowMasterServer;
                                masterServerList[currentRow]=emptyRowMasterServer;
                                break;
                            }
                        }
                    }
                }else{
                    ui->tableWidget->item(currentRow, 1)->setText("");
                }
                abortUDPConnection();
                ui->progressBar->setValue((int)(100*currentRow/allServersInfo.size()));

            }
            if(allServersInfo[currentRow].players.size()>allServersInfo[currentRow].numPlayers)
            {
                //Reset server's info
                allServersInfo[currentRow].players.clear();
                allServersInfo[currentRow].isSet=false;
                //Reset server's info
            }
        }


    isBusy=false;
}

void Servers::getServerInfo(QStringList s)
{
    int portocol1=getIntFromDatagram(s);
    int portocol2=getIntFromDatagram(s);
    int portocol3=getIntFromDatagram(s);
    int portocol4=getIntFromDatagram(s);
    int portocol5=getIntFromDatagram(s);
    int portocol6=getIntFromDatagram(s);
    int portocol7=getIntFromDatagram(s);
    allServersInfo[currentRow].mode=getIntFromDatagram(s);
    allServersInfo[currentRow].numPlayers=getIntFromDatagram(s);
    allServersInfo[currentRow].numMinRemaining=getIntFromDatagram(s);
    allServersInfo[currentRow].mapName=getStringFromDatagram(s);
    allServersInfo[currentRow].serverDescription=getStringFromDatagram(s);
    allServersInfo[currentRow].maxPlayers=getIntFromDatagram(s);
    allServersInfo[currentRow].masterMode=getIntFromDatagram(s);
    allServersInfo[currentRow].hasPassword=getIntFromDatagram(s);
    allServersInfo[currentRow].type=getIntFromDatagram(s);
    while(currentByteArrayIndex<s.size())
    {
        allServersInfo[currentRow].infoLang.append(getStringFromDatagram(s));
    }
//        qDebug()<<"Read data!";
//        qDebug()<<allServersInfo[currentRow].mode;
//        qDebug()<<allServersInfo[currentRow].numPlayers;
//        qDebug()<<allServersInfo[currentRow].numMinRemaining;
//        qDebug()<<allServersInfo[currentRow].mapName;
//        qDebug()<<allServersInfo[currentRow].serverDescription;
//        qDebug()<<allServersInfo[currentRow].maxPlayers;
//        qDebug()<<allServersInfo[currentRow].masterMode;
//        qDebug()<<allServersInfo[currentRow].hasPassword;
//        qDebug()<<allServersInfo[currentRow].type;
//        qDebug()<<allServersInfo[currentRow].infoLang;
//        qDebug()<<"Read data!";


    QTableWidgetItem *pCell = ui->tableWidget->item(currentRow, 0);
    if(!pCell)
    {
        pCell = new QTableWidgetItem;
        ui->tableWidget->setItem(currentRow, 0, pCell);
    }
    pCell->setText(QString::number(allServersInfo[currentRow].numPlayers)+"/"+QString::number(allServersInfo[currentRow].maxPlayers));

    pCell = ui->tableWidget->item(currentRow, 1);
    if(!pCell)
    {
        pCell = new QTableWidgetItem;
        ui->tableWidget->setItem(currentRow, 1, pCell);
    }
    pCell->setText(mode_names[allServersInfo[currentRow].mode+1]);

    pCell = ui->tableWidget->item(currentRow, 2);
    if(!pCell)
    {
        pCell = new QTableWidgetItem;
        ui->tableWidget->setItem(currentRow, 2, pCell);
    }
    pCell->setText(allServersInfo[currentRow].mapName);

    pCell = ui->tableWidget->item(currentRow, 3);
    if(!pCell)
    {
        pCell = new QTableWidgetItem;
        ui->tableWidget->setItem(currentRow, 3, pCell);
    }
    pCell->setText(allServersInfo[currentRow].serverDescription);
    allServersInfo[currentRow].isInfoSet=true;

    secondConnection();
}

void Servers::getPlayerInfo(QStringList s)
{
    currentByteArrayIndex=6;
    if(s.at(currentByteArrayIndex)!="0xF6 ")
    {
        currentByteArrayIndex=currentByteArrayIndex+1;
        int cnNumber=getIntFromDatagram(s);
        int ping;
        if(s.at(currentByteArrayIndex)=="0x80 ")
        {
            currentByteArrayIndex=currentByteArrayIndex+1;
            ping=getIntFromDatagram(s);
            currentByteArrayIndex=currentByteArrayIndex+1;
        }else{
            ping=getIntFromDatagram(s);
        }

        QString player=getStringFromDatagram(s);

        if(isPlayerNameUnique(player,allServersInfo[currentRow].players))
        {
            allServersInfo[currentRow].players.append(player);
        }
    }
}

void Servers::secondConnection()
{
    QByteArray datagramSecond;
    datagramSecond.resize(3);
    datagramSecond[0]=0x00;
    datagramSecond[1]=0x01;
    datagramSecond[2]=0xFF;

    isInFirstResponse=false;

    QString server=masterServerList.at(currentRow);
    QStringList info=server.split(":");
    QString address=info.at(0);
    QString port=info.at(1);

    udpSocket->writeDatagram(datagramSecond, QHostAddress(address), port.toUInt()+1);

}

int getIntFromDatagram(QStringList s)
{
    bool ok;
    int hex = s.at(currentByteArrayIndex).toInt(&ok,16);
    currentByteArrayIndex=currentByteArrayIndex+1;
    return hex;
}

QString getStringFromDatagram(QStringList s)
{
    QString output="";
    while(s.at(currentByteArrayIndex)!="0x00 ")
    {
        if(s.at(currentByteArrayIndex)=="0x0C ")
        {
            currentByteArrayIndex=currentByteArrayIndex+2;
        }else{
            bool ok;
            int hex = s.at(currentByteArrayIndex).toInt(&ok,16);
            output.append((char)hex);
            currentByteArrayIndex=currentByteArrayIndex+1;
        }
    }
    currentByteArrayIndex=currentByteArrayIndex+1;
    return output;
}

void Servers::downloadFinished(QNetworkReply *reply)
{
    QUrl url = reply->url();
    if (reply->error()) {
        fprintf(stderr, "Download of %s failed: %s\n",
                url.toEncoded().constData(),
                qPrintable(reply->errorString()));
        qDebug()<<"Error on reply";
        qDebug()<<"Failed to check for update, no reply from server, trying to check next source...";
        return;
    } else {
        QByteArray output=reply->readAll();

        QString tempAllString = QString::fromLatin1(output.data());

        QStringList servers=tempAllString.split("addserver ");
        for(int i=0;i<servers.size();i++)
        {
            QString server=servers.at(i);
            if(server.length()>0)
            {
                QStringList details=server.split(" ");
                QString final=details.at(0)+":"+details.at(1);
                final=final.split("\n").at(0);
                masterServerList.append(final);
                qDebug()<<final;
            }
        }
        ui->tableWidget->setRowCount(masterServerList.size());
        for (int i=0;i<ui->tableWidget->rowCount();i++){
            QTableWidgetItem *pCell = ui->tableWidget->item(i, 4);
            if(!pCell)
            {
                pCell = new QTableWidgetItem;
                ui->tableWidget->setItem(i, 4, pCell);
            }
            pCell->setText(masterServerList.at(i).split(':').at(0));

            pCell = ui->tableWidget->item(i, 5);
            if(!pCell)
            {
                pCell = new QTableWidgetItem;
                ui->tableWidget->setItem(i, 5, pCell);
            }
            pCell->setText(masterServerList.at(i).split(':').at(1));
        }
    }
    allServersInfo.resize(masterServerList.size());
    currentNumberUnsetServers=masterServerList.size();
    currentDownloads.removeAll(reply);
    reply->deleteLater();

    refreshTimer->start(100);
}

void Servers::sslErrors(const QList<QSslError> &sslErrors)
{
#ifndef QT_NO_SSL
    foreach (const QSslError &error, sslErrors)
    {
        fprintf(stderr, "SSL error: %s\n", qPrintable(error.errorString()));
        qDebug() << error.errorString();
    }
#else
    Q_UNUSED(sslErrors);
#endif
}

void Servers::abortUDPConnection()
{
//    qDebug()<<"ABORTED!";
    if(isBusy==false)
    {
        isBusy=true;
        udpSocket->abort();
        isInFirstResponse=true;
        if(currentRow<ui->tableWidget->rowCount())
        {
            buttomPlainTextViewString="";

            buttomPlainTextViewString.append(allServersInfo[currentRow].serverDescription);
            buttomPlainTextViewString.append("\n");
            for(int i=0;i<allServersInfo[currentRow].players.size();i++)
            {
                buttomPlainTextViewString.append(allServersInfo[currentRow].players.at(i)+"   ");
            }

            ui->plainTextEdit->document()->setPlainText(buttomPlainTextViewString);

            if(allServersInfo[currentRow].isSet==true && refreshTimer->isActive())
            {
//                qDebug()<<"NEXT SERVER";
                nextServer();
            }else if(allServersInfo[currentRow].isSet==false)
            {
//                qDebug()<<"NOT SET";
                currentTotallRefreshTime=currentTotallRefreshTime+10;
                currentRefreshNumberOneServer=currentRefreshNumberOneServer+1;
            }
            if(currentRefreshNumberOneServer>3)
            {
                if(allServersInfo[currentRow].isInfoSet==false)
                {
                    allServersInfo[currentRow].isSet=true;
                }
                nextServer();
            }

            abortTimer->stop();
        }

        isBusy=false;
    }

}

void Servers::nextServer()
{
    if(currentRow<ui->tableWidget->rowCount()-1)
    {
//        qDebug()<<"ADDING TO ROW";
        currentRow=currentRow+1;
        qDebug()<<currentRow;
        currentTotallRefreshTime=80;
        currentRefreshNumberOneServer=0;
    }
}

void Servers::checkConnectionStatus()
{
    currentRefreshNumber=currentRefreshNumber+1;
//    qDebug()<<"Current Row:";
//    qDebug()<<currentRow;
//    qDebug()<<currentTotallRefreshTime;
    if(currentRefreshNumber>totalTimeOutNumberRefreshes)
    {
        qDebug()<<"Timeout";
        on_pushButton_7_clicked();
        return;
    }
//    qDebug()<<udpSocket->state();
    if(udpSocket->state()==QAbstractSocket::UnconnectedState)
    {
        if(currentRow<ui->tableWidget->rowCount() && allServersInfo[allServersInfo.size()-1].isSet==false)
        {
//            qDebug()<<"CLICKED";
            on_tableWidget_cellPressed(currentRow,1);
        }
//        qDebug()<<allServersInfo.size()-1;
//        qDebug()<<currentRow;
//        qDebug()<<ui->tableWidget->rowCount();
        if(currentRow==ui->tableWidget->rowCount()-1 && allServersInfo[allServersInfo.size()-1].isSet==true)
        {
            if(checkServersCompletness())
            {
                qDebug()<<"Finished";
                on_pushButton_7_clicked();
                ui->progressBar->setValue(100);
            }else{
//                qDebug()<<"RESETTING ROW";
                currentRow=0;
            }
        }
    }
}

bool Servers::checkServersCompletness()
{
    bool output=true;
    int numUnset=0;
    for(int i=0;i<masterServerList.size();i++)
    {
        if(allServersInfo[i].isSet==false)
        {
            numUnset=numUnset+1;
            output=false;
        }
    }
    currentNumberUnsetServers=numUnset;
    if(hasChangeGained==true)
    {
        hasChangeGained=false;
        currentTotallRefreshTime=80;
    }else{
        currentTotallRefreshTime=currentTotallRefreshTime+20;
    }
    return output;
}

bool Servers::isPlayerNameUnique(QString name,QStringList nameList)
{
    for(int i=0;i<nameList.size();i++)
    {
        if(nameList.at(i)==name)
        {
            return false;
        }
    }
    return true;
}

void Servers::bubbleSort()
{

}


void Servers::doubleConnection()
{
    on_tableWidget_cellPressed(currentRow,1);
}

void Servers::on_pushButton_5_clicked()
{
//    qDebug()<<"REFRESH BUTTON";
    abortUDPConnection();
    ui->progressBar->setValue(0);
    currentRefreshNumber=0;
    currentTotallRefreshTime=80;
    currentRow=0;
    currentRefreshNumberOneServer=0;

    ui->tableWidget->setEnabled(false);
    ui->pushButton_7->setEnabled(true);
    ui->pushButton_5->setEnabled(false);
    for(int i=0;i<allServersInfo.size();i++)
    {
        allServersInfo[i].isSet=false;
        allServersInfo[i].players.clear();
    }
    refreshTimer->start(100);
}

void Servers::on_pushButton_7_clicked()
{
    qDebug()<<"Canceled";
    refreshTimer->stop();
    abortUDPConnection();
    ui->tableWidget->setEnabled(true);
    ui->pushButton_7->setEnabled(false);
    ui->pushButton_5->setEnabled(true);
}

Servers::~Servers()
{
    delete ui;
}
