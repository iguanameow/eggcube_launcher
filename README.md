# README #

EggCube game launcher for AssaultCube type games made with QT5.

## Related Projects ##
+ EggCube Mod - **[bitbucket.org/iguanameow/eggcube_mod](https://bitbucket.org/iguanameow/eggcube_mod)**
+ EggCube Lua - **[bitbucket.org/iguanameow/eggcube_lua](https://bitbucket.org/iguanameow/eggcube_lua)**

###### Image
![alt text](https://i.imgur.com/ryh9sF3.png "Launcher")

###### Quick summary
QT Creator project files and packaging used to create a launcher for AssaultCube related games.

###### Version
1.1b

##### **Downloads and Updates**
**[Bitbucket](https://bitbucket.org/iguanameow/eggcube_launcher/downloads)** | **[Github](https://github.com/IguanaMeow/eggcube_launcher/releases)**

###### TO DO
1. Impliment add, edit, delete of masterserver, serverlist, and player search - In Progress.
2. SHA2 hashes for next release.
3. Translations to other languages: Japanese, Thai, Hebrew (Google translate is used currently).
4. Fix bug in OSX for not reading and writing to configuration file, or find an OSX developer.
5. Fix bug in Windows for not reading extra arguments passed to added programs.


### How do I get set up? ###

###### Requierments
+ Copy of the AssaultCube game or its forks, 
+ Standalone [QT 5](https://download.qt.io/archive/qt/) and [QT Creator](https://download.qt.io/archive/qtcreator/), or a compatible [Qt online installer](https://download.qt.io/archive/online_installers/), or run install-dependencies.sh for Linux, 
+ [MinGW](http://mingw.org/) for Windows and 32-bit Windows to build for XP,
+ [7-zip](http://7-zip.org/) for Windows, [Keka](http://www.kekaosx.com/) for OSX, and p7zip for Linux.

###### System Requierments
Windows 2000 32-bit or higher, Linux 32/64-bit, OSX 64-bit.

###### Testing & Deployment
1. install-dependencies.sh,
1. Qmake before building and running the project,
2. Build and Run the project via build.sh or QtCreator,
3. Release must be **[packaged](https://bitbucket.org/iguanameow/assaultcube_launcher/src/f8215ddf8cf8d87d752b1d11feee8c4c9a42ed2d/Deployment/PACKAGING/)** to include binary and libraries. See **[deployment](https://bitbucket.org/iguanameow/assaultcube_launcher/src/c4697bb04b66d85b898ae36eab6bb966ba01aa14/Deployment/)** directory of this project.

###### Extracting
+ Use 7-zip's program interface in Windows and OSX to extract. 
+ For Linux either either use an interface or perform this command:
```
7z x Linux64_AC_Launcher_v1.1.7z
```

###### SHA256 Hashes
```
b48164c4f2bb16bf30cd4b941bf7e37c1b4d6f5d656b1802e06b92b027eaa186  Linux32_EggCube_Launcher_1.1b.7z
fa95032288d35c31a0b777ae7244c98858d78579640aaed8df7ce796d7c2f3a6  Linux64_EggCube_Launcher_v1.1b.7z
585f0fe61441a13cb1f8f854773fdd9d42e7d171de0c607c4b01765c5c6c4806  OSX64_EggCube_Launcher_v1.1b.7z
dc6d9f6d2f1640a920d2ff241763ac0ddc4f1af3014122a4dfe6e628ee39f4dc  Win32_EggCube_Launcher_v1.1b.7z
```

###### SHA1 Hashes
```
7452ade99f3b0bce204b176b27c5a22d5addedf9  Linux32_AC_Launcher_v1.0.7z
435e03dc02b463a4c6521bde51b436d9e2efa0d9  Linux64_AC_Launcher_v1.0.7z
5953cd2e27bb015f89bdf456f09a3961c0ae0047  OSX64_AC_Launcher_v1.0.7z
d07474a27b7d292fd1026c9086e8fa9cc15e9f2b  Win32_AC_Launcher_v1.0.7z
```

###### License
[LGPL 2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt)


### Who do I talk to? ###

###### Contact
Repo owner or contributing project members.
