#ifndef SERVERS_H
#define SERVERS_H

#include <QDialog>
#include <QNetworkReply>

namespace Ui {
class servers;
}

class Servers : public QDialog
{
    Q_OBJECT

public:
    explicit Servers(QWidget *parent = 0);
    QNetworkAccessManager *manager;
    QList<QNetworkReply *> currentDownloads;
    ~Servers();

private slots:
    void downloadFinished(QNetworkReply *reply);

    void sslErrors(const QList<QSslError> &sslErrors);

    void on_tableWidget_cellPressed(int row,int column);

    void readUDPData();

    void secondConnection();

    void abortUDPConnection();

    void doubleConnection();

    void checkConnectionStatus();

    void on_pushButton_5_clicked();

    void on_pushButton_7_clicked();

    bool checkServersCompletness();

    bool isPlayerNameUnique(QString name,QStringList nameList);

    void bubbleSort();

    void getServerInfo(QStringList s);

    void getPlayerInfo(QStringList s);

    void nextServer();

private:
    Ui::servers *ui;
    QTimer *abortTimer;
    QTimer *secondTimer;
    QTimer *refreshTimer;
};

#endif // SERVERS_H
