#include "launcher.h"
#include "ui_launcher.h"
#include "edit.h"
#include "ui_edit.h"
#include "add.h"
#include "ui_add.h"
#include "servers.h"
#include "ui_servers.h"
#include "QProcess"
#include "QDesktopServices"
#include "QUrl"
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QTranslator>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrl>
#include <QDateTime>
#include <QTimer>

QStringList nameOfProgram;
QStringList path;
QStringList commands;
QTranslator translator;
QString lastLang;
QNetworkAccessManager *manager;
QList<QNetworkReply *> currentDownloads;
QDate lastDate;
QStringList updateLinks;

int currentUpdateHostIndex=0;

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::launcher)
{
    ui->setupUi(this);

    // Onlne release version
    updateLinks.append("https://bitbucket.org/iguanameow/assaultcube_launcher/downloads/version.txt"); //1.0
    updateLinks.append("https://raw.githubusercontent.com/IguanaMeow/launcher/master/version.txt"); //1.0

    readConfigFile();
    refreshCombobox();

    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),SLOT(downloadFinished(QNetworkReply*)));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(connectionTimeOut()));

    // --------------- Language ComboBox --------------- //
    if(lastLang!=NULL)
    {
        qDebug("Loading last time language.");
        bool langFound=false;
        for(int i=0;i<ui->comboBox_2->count();i++)
        {
            if(ui->comboBox_2->itemText(i)==lastLang)
            {
                ui->comboBox_2->setCurrentIndex(i);
                langFound=true;
            }
        }
        if(langFound==false)
        {
            qDebug("Combobox doesn't have the option!");
        }
    }else{
        QString system_locale = QLocale::system().name();
        qDebug("System default language.");
        qDebug("Searching for file " + system_locale.toLatin1() + ".qm ...");

        QString startupLang = system_locale.split("_").at(0);
        if (translator.load(":/Translations/" + startupLang + ".qm")) {
            qDebug("The translation file was found.");
            bool langFound=false;
            for(int i=0;i<ui->comboBox_2->count();i++)
            {
                if(ui->comboBox_2->itemText(i)==startupLang)
                {
                    ui->comboBox_2->setCurrentIndex(i);
                    langFound=true;
                }
            }
            if(langFound==false)
            {
                qDebug("Combobox doesn't have this option!");
            }
        } else {
            qDebug("The translation file was not found, using english as default.");
        }
    }

    // --------------- Network Notification Date and Version Checking --------------- //
    if(lastDate.isNull()==false)
    {
        QDate now=QDate::currentDate();
        int passedDays=lastDate.daysTo(now);
        if(passedDays>3)
        {
            updateCheck();
        }
    }

 }

// --------------- Network Notification Updater --------------- //
void Launcher::updateCheck()
{
    QString rawURL=updateLinks[currentUpdateHostIndex];

    QUrl url = QUrl::fromEncoded(rawURL.toLocal8Bit());
    doDownload(url);
}

void Launcher::on_comboBox_2_currentIndexChanged()
{
    QString lang=ui->comboBox_2->currentText();
    if(lang=="en")
    {
        qApp->removeTranslator(& translator);
        ui->retranslateUi(this);
    }else{
        qApp->removeTranslator(& translator);
        qDebug()<<translator.load(":/Translations/"+lang);
        qDebug()<<" for loading the language";
        qDebug()<<qApp->installTranslator(& translator);
        qDebug()<<" for installing the language";
        ui->retranslateUi(this);
    }
}

// --------------- Game Selection ComboBox --------------- //
void Launcher::refreshCombobox()
{
    ui->comboBox->clear();
    if(nameOfProgram.size()>0)
    {
        for(int i=0;i<nameOfProgram.size();i++)
        {
            ui->comboBox->addItem(nameOfProgram[i]);
        }
    }
}

// --------------- Read Config File --------------- //
 void Launcher::readConfigFile()
 {
    #ifdef Q_OS_MACOS
        QFile file("../../../ConfigFile.txt");
    #else
        QFile file("ConfigFile.txt");
     #endif
     bool isExist=file.open(QIODevice::ReadOnly);   // Check if config file exists
     if(isExist==true)  // If config file exists
     {
         QTextStream in(&file);
         int numCases=0;
         while(!in.atEnd()) {
             QString line = in.readLine();
             QStringList fields=line.split(",");
             if(fields.at(0)=="$")  // Read global variables
             {
                 if(fields.at(1)=="numCases")
                 {
                     numCases=fields.at(2).toInt();
                 }
                 if(fields.at(1)=="lastLang")
                 {
                     lastLang=fields.at(2);
                 }
                 if(fields.at(1)=="lastDate")
                 {
                     QString temp=fields.at(2);
                     lastDate=QDate(temp.split("/")[0].toInt(),temp.split("/")[1].toInt(),temp.split("/")[2].toInt());
                 }
             }else  // Read instances
             {
                 if(fields.at(1)=="name")
                 {
                     nameOfProgram.append(fields.at(2));
                 }else if(fields.at(1)=="path")
                 {
                     path.append(fields.at(2));
                 }else if(fields.at(1)=="command")
                 {
                     commands.append(fields.at(2));
                 }
             }
         }
         file.close();
     }else{ // If config file does not exist, make new config file
        #ifdef Q_OS_MACOS
            QFile file("../../../ConfigFile.txt");
        #else
            QFile file("ConfigFile.txt");
        #endif
        file.open(QIODevice::WriteOnly);
        QTextStream out(&file);
        out << "$,numCases,0";
     }

 }

Launcher::~Launcher()
{
    lastLang=ui->comboBox_2->currentText();
    writeConfigFile();
    delete ui;
}

// --------------- Add Game --------------- //
void Launcher::on_pushButton_clicked()  // Add Button
{
    qDebug("Add Game to Launcher");
    Add add;
    add.setInputStrings(nameOfProgram,path,commands);   // Send data into another class
    add.setModal(true);
    add.exec();
    refreshCombobox();
}

// --------------- Edit Game --------------- //
void Launcher::on_pushButton_2_clicked()    // Edit Button
{
    qDebug("Edit Game Launcher");
    Edit edit;
    int selection=ui->comboBox->currentIndex(); // Get game selection index

    if(selection>-1)
    {
        edit.setInputStrings(nameOfProgram,path,commands,selection);    //Send data into another class
        edit.setModal(true);
        edit.exec();
        refreshCombobox();
    }else{  // If no game selected

    }
}

// --------------- Delete Game --------------- //
void Launcher::on_pushButton_3_clicked()    // Delete Button
{
    qDebug("Delete Game from Launcher");
    int selection=ui->comboBox->currentIndex(); // Get game selection index
    if(selection!=-1)
    {
        QStringList edittedNameOfProgram;
        QStringList edittedPath;
        QStringList edittedCommands;
        for(int i=0; i<nameOfProgram.size(); i++)   // Refill all data
        {
            if(i!=selection)    // Copy not-selected data
            {
                edittedNameOfProgram.append(nameOfProgram[i]);
                edittedPath.append(path[i]);
                edittedCommands.append(commands[i]);
            }
        }
        nameOfProgram=edittedNameOfProgram;
        path=edittedPath;
        commands=edittedCommands;
        refreshCombobox();
        writeConfigFile();
    }
}

// --------------- Show Servers --------------- //
void Launcher::on_pushButton_6_clicked()
{
    qDebug("Showing Servers");
    Servers servers;
    servers.setModal(true);
    servers.exec();
}

// --------------- Notification Config File Saving --------------- //
void Launcher::writeConfigFile()    // Write back to config file
{
    #ifdef Q_OS_MACOS
        QFile file("../../../ConfigFile.txt");
    #else
        QFile file("ConfigFile.txt");
    #endif
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    QDateTime now = QDateTime::currentDateTime();
    out << "$,lastDate,"+QString::number(now.date().year())+"/"+QString::number(now.date().month())+"/"+QString::number(now.date().day()) << endl; // Writing header
    out << "$,lastLang,"+lastLang << endl; // Writing header
    out << "$,numCases,"+QString::number(nameOfProgram.size()) << endl; // Writing header
    for(int i=0;i<nameOfProgram.size();i++)    // Rewrite all data
    {
        out << QString::number(i)+",name,"+nameOfProgram[i] << endl;
        out << QString::number(i)+",path,"+path[i] << endl;
        out << QString::number(i)+",command,"+commands[i] << endl;
    }
    file.close();
}

// --------------- Play Game --------------- //
void Launcher::on_pushButton_4_clicked()    // Play Button
{
    qDebug("Starting selected Game...\nClosing launcher.");
    int selection=ui->comboBox->currentIndex(); // Get game selection index
    if(selection!=-1)
    {
        QString finalString;    // Set current path to selected path
        QFile f(path[selection]);
        QFileInfo fileInfo(f.fileName());
        QString fileName(fileInfo.fileName());
        QString filePath(fileInfo.path());
        if (!QDir::setCurrent(filePath))
        {
            qDebug("Could not change the current working directory");
        }

        if(commands[selection].size()>0)
        {
            finalString = fileName + " " + commands[selection];
        }else{
            finalString = fileName;
        }
        qDebug() << "./" + finalString;  // start program from the selected path
        bool success=QProcess::startDetached("./"+finalString);
        QApplication::quit();
    }else{
        qDebug("Nothing selected!");
    }
}

// --------------- Network Notification Updater --------------- //
void Launcher::doDownload(const QUrl &url)
{
    QNetworkRequest request(url);
    QNetworkReply *reply = manager->get(request);

#ifndef QT_NO_SSL
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), SLOT(sslErrors(QList<QSslError>)));
#endif

    qDebug()<<url;
    currentDownloads.append(reply);

    timer->start(5000);
}

void Launcher::downloadFinished(QNetworkReply *reply)
{
    QUrl url = reply->url();
    QFile file("updateFile");
    bool isUpdateCheckSuccess=false;
    if (reply->error()) {
        fprintf(stderr, "Download of %s failed: %s\n",
                url.toEncoded().constData(),
                qPrintable(reply->errorString()));
        qDebug()<<"Error on reply";
        qDebug()<<"Failed to check for update, no reply from server, trying to check next source...";
        if(currentUpdateHostIndex<updateLinks.size()-1)
        {
            currentUpdateHostIndex=currentUpdateHostIndex+1;
            updateCheck();
        }else{
            qDebug()<<"No other update sources found!";
            QPixmap mypix (":/Images/question.png");
            ui->label_3->setPixmap(mypix);
        }
        return;
    } else {
        qDebug()<<reply;

        if (!file.open(QIODevice::WriteOnly)) {
                fprintf(stderr, "Could not open %s for writing: %s\n",
                        qPrintable("updateFile"),
                        qPrintable(file.errorString()));
            }
        file.write(reply->readAll());
        file.flush();
        file.close();
    }
    currentDownloads.removeAll(reply);
    reply->deleteLater();

    bool isExist=file.open(QIODevice::ReadOnly);   // Check if config file exists
    if(isExist==true)  // If config file exists
    {
        QTextStream in(&file);
        while(!in.atEnd()) {
            QString line = in.readLine();
            qDebug()<<line.trimmed();
            if (line.trimmed().isEmpty())
            {
                qDebug()<<"Failed to check for update, empty download, trying to check next source...";
                file.close();
                if(currentUpdateHostIndex<updateLinks.size()-1)
                {
                    currentUpdateHostIndex=currentUpdateHostIndex+1;
                    updateCheck();
                }else{
                    qDebug()<<"No other update sources found!";
                    QPixmap mypix (":/Images/question.png");
                    ui->label_3->setPixmap(mypix);
                }
            }else{
                float lastVer=line.toFloat();
                float currentVer=ui->label->text().split(" ")[1].toFloat();
                qDebug()<<"Current verssion: "+QString::number(currentVer);
                if(currentVer<lastVer)
                {
                    qDebug()<<"Update Avalible!";
                    QPixmap mypix (":/Images/bad.png");
                    ui->label_3->setPixmap(mypix);
                    QPixmap mypixel (":/Images/warn.png");
                    ui->menuUpdates->setIcon(mypixel);
                    isUpdateCheckSuccess=true;
                }
                file.close();
            }
        }
    }else{
        qDebug()<<"Update file is missing!\nNo update file downloaded.";
    }
    qDebug()<<"Update check finished with success:"+QString::number(isUpdateCheckSuccess);
}

void Launcher::sslErrors(const QList<QSslError> &sslErrors)
{
#ifndef QT_NO_SSL
    foreach (const QSslError &error, sslErrors)
    {
        fprintf(stderr, "SSL error: %s\n", qPrintable(error.errorString()));
        qDebug() << error.errorString();
    }
#else
    Q_UNUSED(sslErrors);
#endif
}

void Launcher::connectionTimeOut()
{
    if(currentDownloads.size()>0)
    {
        currentDownloads.at(0)->abort();
        currentDownloads.at(0)->deleteLater();
        currentDownloads.clear();

        qDebug()<<"Failed to check for update, reply timeout, trying to check next source...";
        if(currentUpdateHostIndex<updateLinks.size()-1)
        {
            currentUpdateHostIndex=currentUpdateHostIndex+1;
            updateCheck();
        }else{
            qDebug()<<"No other update sources found!";
            QPixmap mypix (":/Images/question.png");
            ui->label_3->setPixmap(mypix);
        }
    }

}

// --------------- Launcher Menus --------------- //

// ------------------ IRC ------------------ //
void Launcher::on_pushButton_5_clicked()    // IRC Button
{
    QDesktopServices::openUrl(QUrl("http://185.22.174.73:9000"));
    qDebug("Opening IRC");
}

// --------------- Launcher Menues --------------- //
void Launcher::on_actionAssaultCube_triggered()
{
    QDesktopServices::openUrl(QUrl("http://assault.cubers.net/"));
    qDebug("Opening Browser to AC");
}

void Launcher::on_actionGema_Forums_triggered()
{
    QDesktopServices::openUrl(QUrl("http://gema.forumactif.com/"));
    qDebug("Opening Browser to Gema");
}

void Launcher::on_actionIguana_Site_triggered()
{
    QDesktopServices::openUrl(QUrl("http://185.22.174.73/"));
    qDebug("Opening Browser to Iguana");
}

void Launcher::on_actionDocs_triggered()
{
    QDesktopServices::openUrl(QUrl("http://assault.cubers.net/docs"));
    qDebug("Opening Browser to Docs");
}

void Launcher::on_actionWiki_triggered()
{
    QDesktopServices::openUrl(QUrl("http://wiki.cubers.net/"));
    qDebug("Opening Browser to Wiki");
}

void Launcher::on_actionAkimbo_triggered()
{
    QDesktopServices::openUrl(QUrl("http://ac-akimbo.net/"));
    qDebug("Opening Browser to Akimbo");
}

void Launcher::on_actionQuadropolis_triggered()
{
    QDesktopServices::openUrl(QUrl("http://quadropolis.us/taxonomy/term/24"));
    qDebug("Opening Browser to Quadropolis");
}

void Launcher::on_actionAkimbo_US_triggered()
{
    QDesktopServices::openUrl(QUrl("http://us.ac-akimbo.net/packages/"));
    qDebug("Opening Browser to Akimbo US");
}

void Launcher::on_actionIguana_RU_triggered()
{
    QDesktopServices::openUrl(QUrl("http://185.22.174.73/~iguana/packages/"));
    qDebug("Opening Browser to Iguana RU");
}

void Launcher::on_actiontm_FR_triggered()
{
    QDesktopServices::openUrl(QUrl("http://164.132.110.143/packages/"));
    qDebug("Opening Browser to tm FR");
}

void Launcher::on_actionAssaultCube_2_triggered()
{
    QDesktopServices::openUrl(QUrl("https://webchat.quakenet.org/?channels=assaultcube&uio=MTY9bnVsbCYxMz10cnVl85"));
    qDebug("Opening Browser to AC IRC");
}

void Launcher::on_actionLauncher_triggered()
{
    QDesktopServices::openUrl(QUrl("https://bitbucket.org/iguanameow/assaultcube_launcher"));
    qDebug("Opening Browser to Launcher");
}

void Launcher::on_actionDownload_1_triggered()
{
    QDesktopServices::openUrl(QUrl("https://bitbucket.org/iguanameow/assaultcube_launcher/downloads"));
    qDebug("Opening Browser to Download 1 Bitbucket");
}

void Launcher::on_actionDownload_2_triggered()
{
    QDesktopServices::openUrl(QUrl("https://github.com/IguanaMeow/launcher/releases"));
    qDebug("Opening Browser to Download 2 Github");
}

void Launcher::on_actionQt_triggered()
{
    QDesktopServices::openUrl(QUrl("https://www.qt.io/"));
    qDebug("Opening Browser to Qt");
}
