<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open Game Path</source>
        <translation>게임 폴더 열기</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="117"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>게임 폴더 열기</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>런처 추가</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>경로</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>설정:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>런처 수정</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>경로</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>설정:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>이름:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>EggCube Launcher</source>
        <translation>EggCube 런처</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>추가 +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>수정 =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>실행 &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>채팅 IRC</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>삭제 -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>사이트</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="458"/>
        <source>Documents</source>
        <translation>문서</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="465"/>
        <source>Content</source>
        <translation>컨텐츠</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="472"/>
        <source>Packages</source>
        <translation>패키지</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="480"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="506"/>
        <source>Docs</source>
        <translation>문서</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="511"/>
        <source>Wiki</source>
        <translation>위키</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="516"/>
        <source>Gema Forums</source>
        <translation>Gema 포럼</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="521"/>
        <source>Iguana Site</source>
        <translation>Iguana 사이트</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <location filename="../launcher.ui" line="551"/>
        <source>Launcher</source>
        <translation>런처</translation>
    </message>
</context>
</TS>
