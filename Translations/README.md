Translation files are in this directory.

Use the Qt [Linguist](https://doc.qt.io/qt-5.8/linguist-translators.html) to edit translation .ts files. 
Then [update and produce](https://doc.qt.io/qt-5.8/linguist-manager.html) binary translated .qm files for Qt.
```
/path/to/Qt/bin/lupdate /path/to/project.pro
/path/to/Qt/bin/lrelease /path/to/project.pro
```

Terms for translation:
```
Open AssaultCube
Open Game Path
Launcher Add
Launcher Edit
Add
Edit
Delete 
Play
Path 
Options 
Name
Save
QT AssaultCube Launcher
Chat on IRC
Sites
Documents
Content
Packages
About us
Docs
Wiki
Gema Forums
Iguana Site
Launcher
```
