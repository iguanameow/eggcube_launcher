<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open Game Path</source>
        <translation>Apri la cartella del gioco</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="117"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>Apri la cartella del gioco</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>Aggiungi Launcher</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>Opzioni:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>Modifica Launcher</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>Opzioni:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>Nome:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>EggCube Launcher</source>
        <translation>EggCube Launcher</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>Aggiungi +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>Modifica =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>Gioca &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>IRC Chat</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>Rimuovi -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>Siti</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="458"/>
        <source>Documents</source>
        <translation>Documenti</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="465"/>
        <source>Content</source>
        <translation>Contenuto</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="472"/>
        <source>Packages</source>
        <translation>Pacchetti</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="480"/>
        <source>About</source>
        <translation>Su di noi</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="506"/>
        <source>Docs</source>
        <translation>Docs</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="511"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="516"/>
        <source>Gema Forums</source>
        <translation>Forum Gema</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="521"/>
        <source>Iguana Site</source>
        <translation>Sito di Iguana</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <location filename="../launcher.ui" line="551"/>
        <source>Launcher</source>
        <translation>Launcher</translation>
    </message>
</context>
</TS>
