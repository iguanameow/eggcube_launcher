<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open Game Path</source>
        <translation>Ouvrir le Chemin du Jeu</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="117"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>Ouvrir le Chemin du Jeu</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>Ajouter le Démarreur</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>Options:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>Édition du Démarreur</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>Sauv</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>Options:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>EggCube Launcher</source>
        <translation>EggCube Démarreur</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>Ajouter +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>Éditer =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>Jouer &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>Ouvrir l&apos;IRC</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>Effacer -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>Sites</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="458"/>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="465"/>
        <source>Content</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="472"/>
        <source>Packages</source>
        <translation>Packages</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="480"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="506"/>
        <source>Docs</source>
        <translation>Docs</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="511"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="516"/>
        <source>Gema Forums</source>
        <translation>Les Forums Gema</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="521"/>
        <source>Iguana Site</source>
        <translation>Le Site d&apos;Iguana</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <location filename="../launcher.ui" line="551"/>
        <source>Launcher</source>
        <translation>Démarreur</translation>
    </message>
</context>
</TS>
